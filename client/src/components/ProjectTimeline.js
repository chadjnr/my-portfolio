import React, { Component } from 'react';
import Timeline from './Timeline'
import ProjectsApi from '../api/ProjectsApi';

class Projects extends Component {
    state = { 
        projects: []
     }

    componentDidMount(){
        ProjectsApi.getProjects()
        .then(
            res=>{
                console.log(res.data)
            }
        )
    }
    render() { 
        return ( 
            <div>
                <Timeline/>
            </div>
         );
    }
}
 
export default Projects;