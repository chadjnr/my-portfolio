import React, { Component } from 'react';
import Blog from './Blog';
import BlogApi from '../api/BlogApi';

class BlogTimeline extends Component {
    state = { 
        blogs: []
     }

     componentDidMount(){
         
         BlogApi.getBlogs()
         .then(
             res=>{console.log(res.data)
             this.setState({blogs: res.data})}
         )
     }
    render() {
        const {blogs}  = this.state;
        const mapblogs = blogs.length ? (blogs.map(blog=>{
            return(
                <Blog blog={blog} key={blog._id}/>
                )
            })
            ): 
            <div>There are no Blogs Yet</div>
        return ( 
            <div>
                {mapblogs}
            </div>
         );
    }
}
 
export default BlogTimeline;