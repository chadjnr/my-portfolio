import axios from 'axios'

const baseUrl = 'http://localhost:8080/project'

class ProjectsApi{

    getProjects(){
        return axios.get(`${baseUrl}`)
    }

}

export default new ProjectsApi()