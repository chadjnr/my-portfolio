import axios from 'axios'

const baseUrl = 'http://localhost:8080/blog'

class BloApi{

    getBlogs(){
        return axios.get(`${baseUrl}`)
    }

}

export default new BloApi();